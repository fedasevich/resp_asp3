﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Xml.Linq;

class Program
{
    static HttpClient client = new HttpClient();

    static void Main(string[] args)
    {
        Thread waiter1 = new Thread(() => ServeCustomer("Waiter 1", "Customer 1"));
        Thread waiter2 = new Thread(() => ServeCustomer("Waiter 2", "Customer 2"));
        Thread waiter3 = new Thread(() => ServeCustomer("Waiter 3", "Customer 3"));

        waiter1.Start();
        waiter2.Start();
        waiter3.Start();

        waiter1.Join();
        waiter2.Join();
        waiter3.Join();

        Task.WaitAll(MainAsync());
    }

    static void ServeCustomer(string waiterName, string customerName)
    {
        Console.WriteLine($"{waiterName} is taking order from {customerName}");
        Thread.Sleep(2000);

        PrepareFood(waiterName, customerName);

        Console.WriteLine($"{waiterName} is billing {customerName}");
        Thread.Sleep(1000);

        CleanTable(waiterName, customerName);
    }

    static void PrepareFood(string waiterName, string customerName)
    {
        Console.WriteLine($"{waiterName} is preparing food for {customerName}");
        Thread.Sleep(3000);

        Console.WriteLine($"{waiterName} is serving food to {customerName}");
        Thread.Sleep(2000);
    }

    static void CleanTable(string waiterName, string customerName)
    {
        Console.WriteLine($"{waiterName} is cleaning the table after {customerName}");
        Thread.Sleep(2000);

        Console.WriteLine($"{waiterName} has finished serving {customerName}");
    }


    static async Task MainAsync()
    {
        var users = await GetUsers();
        var user = users[0];
        Console.WriteLine($"User: {user.Name}");

        var posts = await GetPostsByUser(user.Id);
        var post = posts[0];
        Console.WriteLine($"Post: {post.Title}");

        var comments = await GetCommentsByPost(post.Id);
        var comment = comments[0];
        Console.WriteLine($"Comment: {comment.Body}");
    }

    static async Task<List<User>> GetUsers()
    {
        string responseBody = await client.GetStringAsync("https://jsonplaceholder.typicode.com/users");
        List<User> users = JsonSerializer.Deserialize<List<User>>(responseBody);

        return users;
    }

    static async Task<List<Post>> GetPostsByUser(int userId)
    {
        string responseBody = await client.GetStringAsync($"https://jsonplaceholder.typicode.com/users/{userId}/posts");
        List<Post> posts = JsonSerializer.Deserialize<List<Post>>(responseBody);
        return posts;
    }

    static async Task<List<Comment>> GetCommentsByPost(int postId)
    {
        string responseBody = await client.GetStringAsync($"https://jsonplaceholder.typicode.com/posts/{postId}/comments");
        List<Comment> comments = JsonSerializer.Deserialize<List<Comment>>(responseBody);
        return comments;
    }
}


public class User
{
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("name")]
    public string Name { get; set; }
}

public class Post
{
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("title")]
    public string Title { get; set; }
}

public class Comment
{
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("body")]
    public string Body { get; set; }
}